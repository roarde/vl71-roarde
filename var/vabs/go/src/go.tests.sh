#!/bin/bash

cat >> /tmp/hello-world.go <<EFO
package main

import "fmt"
func main() {
	fmt.Println("Hello world!")
}
EFO

go run /tmp/hello-world.go || exit 1
rm /tmp/hello-world.go
