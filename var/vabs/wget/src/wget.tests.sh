#!/bin/bash

CWD=$(pwd)

NORUN=1 . $CWD/wget.SlackBuild || exit 1

printf "Download a test file ..."
wget -q --no-check-certificate $LINK -O /tmp/wget-test-download || { printf "FAILED ... \n"; exit 1; }
printf "PASS \n"
# Now compare the md5sum of the download.
printf "Verify downloaded file ..."

oldsum=$(md5sum $CWD/$(basename $LINK) | cut -f 1 -d ' '|xargs)
newsum=$(md5sum /tmp/wget-test-download | cut -f 1 -d ' '|xargs)

if [ "$oldsum" != "$newsum" ]; then
	printf "FAILED \n"
	exit 1
fi
printf "PASS \n"
