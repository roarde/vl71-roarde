#!/bin/sh

# Copyright 2006, 2007, 2008, 2009, 2010, 2012  Patrick J. Volkerding, Sebeka, MN, USA
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


NAME=net-tools
VERSION=${VERSION:-1.60.20120726git}
BUILDNUM=${BUILDNUM:-"1"}
VL_VERSION=${VL_VERSION:-"$(ls /var/log/packages/|grep vlconfig2|cut -d "-" -f4|cut -c 2-5)"}
BUILD=${BUILD:-"$BUILDNUM""$VL_VERSION"}
LINK=${LINK:-"http://slackware.osuosl.org/slackware-current/source/n/$NAME/$NAME-$VERSION.tar.xz"}

# Automatically determine the architecture we're building on:
if [ -z "$ARCH" ]; then
  case "$( uname -m )" in
    i?86) export ARCH=i586 ;;
    arm*) export ARCH=arm ;;
    # Unless $ARCH is already set, use uname -m for all other archs:
       *) export ARCH=$( uname -m ) ;;
  esac
fi
#get the source..........
for SRC in $(echo $LINK);do
if [ ! -f $CWD/$(basename $SRC) ]
then
	wget --no-check-certificate -c $SRC
fi
done
CWD=$(pwd)
cd ../
RELEASEDIR=$(pwd)
cd $CWD
mkdir -p $RELEASEDIR/tmp
TMP=$RELEASEDIR/tmp
PKG=$TMP/package-${NAME}
rm -rf $PKG
mkdir -p $TMP $PKG

if [ "$ARCH" = "i386" ]; then
  SLKCFLAGS="-O2 -march=i386 -mcpu=i686"
elif [ "$ARCH" = "i586" ]; then
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
elif [ "$ARCH" = "s390" ]; then
  SLKCFLAGS="-O2"
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fPIC"
else
  SLKCFLAGS="-O2"
fi

cd $TMP
rm -rf net-tools-$VERSION
tar xvf $CWD/net-tools-$VERSION.tar.xz || exit 1
cd net-tools-$VERSION || exit 1

zcat $CWD/net-tools.config.h.gz > config.h
sed -i -e '/Token/s/y$/n/' config.in
yes "" | make config

chown -R root:root .
find . \
  \( -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
  -exec chmod 755 {} \; -o \
  \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
  -exec chmod 644 {} \;

mkdir -p $PKG/usr/doc/net-tools-$VERSION
cp -a COPYING* README* $PKG/usr/doc/net-tools-$VERSION
chmod 644 $PKG/usr/doc/net-tools-$VERSION/*
chown root:root $PKG/usr/doc/net-tools-$VERSION/*
HAVE_IP_TOOLS=1 HAVE_MII=1 make || exit 1
HAVE_IP_TOOLS=1 HAVE_MII=1 make hostname || exit 1
strip --strip-unneeded ipmaddr iptunnel hostname arp ifconfig nameif rarp route netstat plipconfig slattach mii-tool
mkdir -p $PKG/sbin $PKG/bin $PKG/usr/sbin
cat arp > $PKG/sbin/arp
cat ifconfig > $PKG/sbin/ifconfig
cat rarp > $PKG/sbin/rarp
cat route > $PKG/sbin/route
cat hostname > $PKG/bin/hostname
cat mii-tool > $PKG/sbin/mii-tool
cat nameif > $PKG/sbin/nameif
cat netstat > $PKG/bin/netstat
cat plipconfig > $PKG/sbin/plipconfig
cat slattach > $PKG/usr/sbin/slattach
cat ipmaddr > $PKG/sbin/ipmaddr
cat iptunnel > $PKG/sbin/iptunnel
chmod 755 $PKG/sbin/* $PKG/bin/* $PKG/usr/sbin/*
cd man/en_US
mkdir -p $PKG/usr/man/man{1,5,8}
for page in dnsdomainname.1 domainname.1 hostname.1 nisdomainname.1 \
  ypdomainname.1 ; do
  cat $page | gzip -9c > $PKG/usr/man/man1/$page.gz
done
cat ethers.5 | gzip -9c > $PKG/usr/man/man5/ethers.5.gz
for page in arp.8 ifconfig.8 mii-tool.8 nameif.8 netstat.8 rarp.8 route.8 \
  slattach.8 plipconfig.8 ; do
  cat $page | gzip -9c > $PKG/usr/man/man8/$page.gz
done
( cd $PKG/bin
  ln -sf hostname dnsdomainname
  ln -sf hostname nisdomainname
  ln -sf hostname ypdomainname
)

# This is a little Slackware-specific tool used in some of the network
# related scripts to calculate network and broadcast addresses:
( cd $PKG/bin
  cc -O2 -o ipmask $CWD/ipmask.c
  strip --strip-unneeded ipmask
  chmod 755 ipmask
)
cat $CWD/ipmask.8 | gzip -9c > $PKG/usr/man/man8/ipmask.8.gz

mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc

cd $PKG
requiredbuilder -v -y -s $RELEASEDIR $PKG
makepkg -c n -l y $RELEASEDIR/$NAME-$VERSION-$ARCH-$BUILD.txz
rm -rf $TMP


