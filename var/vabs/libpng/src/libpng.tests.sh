#!/bin/bash

printf "Install a package known to need libpng ... \n"

slapt-get -u >/dev/null 2>&1 || exit 1
slapt-get -i gtk+2 || exit 1

printf "Test backwards compatibility of updated libpng ... "
ldd /usr/bin/gtk-demo | grep libpng | grep -i found >/dev/null || { printf "PASS\n"; exit 0; }
printf "FAILED\n"
exit 1


