#!/bin/sh

# Copyright 2006, 2007, 2008, 2009, 2010, 2012  Patrick J. Volkerding, Sebeka, MN, USA
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


NAME=network-scripts
VERSION=${VERSION:-14.00}
ARCH=noarch
BUILDNUM=${BUILDNUM:-"1"}
VL_VERSION=${VL_VERSION:-"$(ls /var/log/packages/|grep vlconfig2|cut -d "-" -f4|cut -c 2-5)"}
BUILD=${BUILD:-"$BUILDNUM""$VL_VERSION"}

CWD=$(pwd)
cd ../
RELEASEDIR=$(pwd)
cd $CWD
mkdir -p $RELEASEDIR/tmp
TMP=$RELEASEDIR/tmp
PKG=$TMP/package-${NAME}

rm -rf $PKG
mkdir -p $TMP $PKG/etc/rc.d
( cd $PKG/etc/rc.d
  for file in rc.inet1.conf rc.inet1 rc.inet2 rc.ip_forward ; do
    cp -a $CWD/scripts/$file ${file}.new
  done
  chown root:root *
  chmod 755 rc.inet1.new rc.inet2.new
  chmod 600 rc.inet1.conf.new
)
( cd $PKG/etc
  for file in HOSTNAME host.conf hosts.allow hosts.deny hosts.equiv hosts networks nntpserver protocols resolv.conf resolv.conf.head; do
    cp -a $CWD/scripts/$file ${file}.new
    chmod 644 ${file}.new
  done
  chown root:root *
)
_ARCH=$(uname -m)
if [[ "$_ARCH" = i?86 ]]; then
  _DISTRO="vector"
elif [ "$ARCH" = "x86_64" ]; then
  _DISTRO="vlocity"
fi

# Fix the hostname related stuff
sed -i "s|@DISTRO@|$_DISTRO|g" $PKG/etc/HOSTNAME.new
sed -i "s|@DISTRO@|$_DISTRO|g" $PKG/etc/hosts.new

mkdir -p $PKG/sbin
cp -a $CWD/scripts/netconfig $PKG/sbin/netconfig
chown root:root $PKG/sbin/netconfig
chmod 755 $PKG/sbin/netconfig
mkdir -p $PKG/var/log/setup
cp -a $CWD/scripts/setup.netconfig $PKG/var/log/setup
chown root:root $PKG/var/log/setup/setup.netconfig
chmod 755 $PKG/var/log/setup/setup.netconfig

# Add manpages:
mkdir -p $PKG/usr/man/man{5,8}
cat $CWD/manpages/rc.inet1.conf.5 | gzip -9c > $PKG/usr/man/man5/rc.inet1.conf.5.gz
cat $CWD/manpages/rc.inet1.8 | gzip -9c > $PKG/usr/man/man8/rc.inet1.8.gz

mkdir -p $PKG/install
zcat $CWD/doinst.sh.gz > $PKG/install/doinst.sh
cat $CWD/slack-desc > $PKG/install/slack-desc

cd $PKG
makepkg -c n -l y $RELEASEDIR/$NAME-$VERSION-$ARCH-$BUILD.txz
rm -rf $TMP
